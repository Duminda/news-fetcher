#Front-end

To build front end

`npm build`

Copy build directory content to back-end public directory.

TODO : Add a Gulp task to build automation, Redux for store management 

#Back-end

Change config.json file and point your mongo instance.

To run the app

`npm start`

App will start on port 3001
