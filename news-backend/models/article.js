"use strict";

module.exports = function(mongoose) {

  var schema = new mongoose.Schema({
        author: String,
        content: String,
        description: String,
        publishedAt: String,
        source: { id: String, name: String },
        title: String,
        url: String,
        urlToImage: String
  })

  var Articles = mongoose.model("Articles", schema);

  return Articles;
};