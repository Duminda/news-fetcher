var express = require('express');
var router = express.Router();
var models = require('../models')

router.get('/', function(req, res, next) {

  var query = models.Articles.find();

  res.setHeader('Content-Type', 'application/json');

  query.exec(function (err, docs) {
    if (err) {
      res.send(JSON.stringify({ message: "ERROR", info: 'no articles found.' }, null, 3));
      console.log(err)
    } else {
      res.send(JSON.stringify(docs, null, 3));
    }
  });

});

router.post('/', function(req, res, next) {

  models.Articles.insertMany(req.body, onInsert);

  res.setHeader('Content-Type', 'application/json');

  function onInsert(err, docs) {
    if (err) {
      res.send(JSON.stringify({ message: "ERROR", info: 'error while saving articles.' }, null, 3));
      console.log(err)
    } else {
      res.send(JSON.stringify({ message: "SUCCESS", info: docs.length + ' articles were successfully stored.' }, null, 3));
    }
}
});

router.get('/:id', function(req, res, next) {

  var query = models.Articles.findById(req.params.id);

  res.setHeader('Content-Type', 'application/json');

  query.exec(function (err, docs) {
    if (err) {
      res.send(JSON.stringify({ message: "ERROR", info: 'no such article found.' }, null, 3));
      console.log(err)
    } else {
      res.send(JSON.stringify(docs, null, 3));
    }
  });

});

module.exports = router;
