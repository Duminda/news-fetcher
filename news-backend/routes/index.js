var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({ title: 'News Store API' }, null, 3));
});


module.exports = router;
