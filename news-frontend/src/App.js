import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'

import './App.css'
import 'bootstrap/dist/css/bootstrap.css'


import Header from './layout/Header';
import NewsViewer from './components/NewsViewer';
import NewsFetcher from './components/NewsFetcher';
import NewsStorer from './components/NewsStorer';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <Header/>
        <Route exact path="/" render={props=>(
          <React.Fragment>
            <NewsFetcher />
          </React.Fragment>
        )}/>   
               
        <Route path="/news" component={NewsViewer}/>
        <Route path="/save" component={NewsStorer}/>
      </div>
      </Router>
    );
  }
}

export default App;
