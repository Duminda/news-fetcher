import React, {Component} from 'react';
import { Link } from 'react-router-dom'
import {Container, Row, Col, Form, Button, Alert} from 'react-bootstrap';

import countries from '../resources/countries.json'
import catagories from '../resources/catagories.json'
import {getSourcesReq, getHeadlinesReq} from '../network/request'

import {CONFIG} from '../constants'

class NewsFetcher extends Component{

    state = {
        sources: [],
        articles: [],
        selectedCountry : '',
        selectedCatagory: '',
        selectedSource: '',
        queryString: '',
        articleCount: 0
    }

    componentDidMount(){
        this.setState({selectedCatagory: catagories[0].name});
        this.setState({selectedCountry: countries[0].code});       
    }

    componentDidUpdate(prevProps, prevState) {

        if (prevState.selectedCatagory !== this.state.selectedCatagory || prevState.selectedCountry !== this.state.selectedCountry) {
            
            let searchString = `category=${this.state.selectedCatagory}&country=${this.state.selectedCountry}&apiKey=${CONFIG.API_KEY}`;
            let that = this;
            getSourcesReq(searchString).then((newSources) => {
                that.setState({sources: newSources});
                that.setState({selectedSource: newSources[0].id});
            })
            .catch((error) => {
                console.log(error);
            })
        }
      }

    
    onChangeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onGetHeadLines = (e) => {
        e.preventDefault();

        let searchString = `sources=${this.state.selectedSource}&q=${this.state.queryString}&apiKey=${CONFIG.API_KEY}`;
        let that = this;
        
        getHeadlinesReq(searchString).then((articleData) => {
            that.setState({articleCount: articleData.totalResults, articles: articleData.articles})
        })
        .catch((error) => {
            console.log(error);
        })

    }

    render() {
      return (
        <Container>
        <Row>&nbsp;</Row>
        <Row>&nbsp;</Row>
        <Row>
            <Col xs={12} md={8}>
                <Form onSubmit={this.onGetHeadLines}>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridCountry">
                        <Form.Label>Country</Form.Label>
                        <Form.Control as="select"  onChange={this.onChangeHandler.bind(this)} name="selectedCountry">
                        {
                            countries.map((countryData) => {
                                return(<option key={countryData.id} value={countryData.code}>{countryData.name}</option>)
                            })
                        }
                            
                        </Form.Control>
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridCatagory">
                        <Form.Label>Catagory</Form.Label>
                        <Form.Control as="select" onChange={this.onChangeHandler.bind(this)} name="selectedCatagory">
                        {
                            catagories.map((catagory) => {
                                return(<option key={catagory.id} value={catagory.name}>{catagory.name}</option>)
                            })
                        }
                        </Form.Control>
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridSource">
                        <Form.Label>Source</Form.Label>
                        <Form.Control as="select" onChange={this.onChangeHandler.bind(this)} name="selectedSource">
                        {
                            this.state.sources.map((source) => {
                                return(<option key={source.id} value={source.id}>{source.name}</option>)
                            })
                        }
                        </Form.Control>
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridQuery">
                        <Form.Label>Keyword</Form.Label>
                        <Form.Control value={this.state.queryString} onChange={this.onChangeHandler.bind(this)} name="queryString"/>
                        </Form.Group>
                    </Form.Row>

                    <Button variant="primary" type="submit">
                        Get Headlines
                    </Button>

                    
                </Form>
            </Col>
            <Col xs={6} md={4}></Col>
            
            </Row>
            <Row>&nbsp;</Row>
            <Row>
            <Col xs={12} md={8}>

            {
                this.state.articleCount > 0 ?
                <Alert variant="primary">
                        {this.state.articleCount} articles found. 
                    <Link to={{
                            pathname: '/save',
                            state: {
                                articles: this.state.articles
                            }         
                    }} > View
                    </Link>
                </Alert>
                :
                <div></div>
            }
            
            </Col>
            <Col xs={6} md={4}></Col>
            
            </Row>
      </Container>
      )
    }

}

export default NewsFetcher;