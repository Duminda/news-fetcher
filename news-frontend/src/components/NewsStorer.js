import React, {Component} from 'react';
import {Container, Row, Col, ListGroup, Alert, Form, Button, Badge} from 'react-bootstrap';

import {postHeadlinesReq} from '../network/request'

class NewsStorer extends Component{

    state = {
        saved: false,
        articlesToSave: []
    }

    componentDidMount () {
        this.setState({saved : false});
        if(this.props.location.state !== undefined){
            const { articles } = this.props.location.state;
            this.setState({articlesToSave: articles});
        }
    }

    onSaveArticles = (e) =>{

        e.preventDefault();

        let that = this;

        postHeadlinesReq(this.state.articlesToSave).then((message) => {
            if(message === "SUCCESS")
                that.setState({saved : true}); 
        })
        .catch((error) => {
            console.log(error);
        })
    }

    render() {
      return (
        <Container>
        <Row>&nbsp;</Row>
            <h1>
                <Badge variant="secondary">Headlines</Badge>
            </h1>       
        <Row>
            <Col xs={12} md={8}>
            <ListGroup>{
                this.state.articlesToSave.length > 0 ?
            this.state.articlesToSave.map((article) => {
                return(<ListGroup.Item key={article.publishedAt}>{article.title}</ListGroup.Item>)
            })
            :
            <Alert variant="danger">
                No articles to save
            </Alert>
            }
            </ListGroup>
            </Col>
            <Col xs={6} md={4}>
            <Form onSubmit={this.onSaveArticles}>
                    <Button variant="primary" type="submit">
                        Save Headlines
                    </Button>
                </Form>
            </Col>
        </Row>
        <Row>
            &nbsp;
        </Row>
        <Row>
            <Col xs={12} md={8}>
            {
                this.state.saved ?
                <Alert variant="primary">
                         Articles saved successfully. 
                </Alert>
                :
                <div></div>
            }
            
            </Col>
            <Col xs={6} md={4}></Col>
            
            </Row>
        </Container>
      )
    }

}

export default NewsStorer;