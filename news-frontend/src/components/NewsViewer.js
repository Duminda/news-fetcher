import React, {Component} from 'react';
import {Container, Row, Col, ListGroup, Badge, Button, Form} from 'react-bootstrap';

import {getStoredHeadlinesReq} from '../network/request'

class NewsViewer extends Component{

  state = {
    articles: [],
    title: '',
    author: '',
    content: '',
    url: '',
    publishedAt: '',
    showDetails: false
  }

  componentDidMount(){
    let that = this;
    getStoredHeadlinesReq(null).then((data) => {
      that.setState({ articles: data})
    })
    .catch((error) => {
        console.log(error);
    }) 
  }

  onArticleClick = (id) => {

    let that = this;
    getStoredHeadlinesReq(id).then((data) => {
      that.setState({
        title : data.title,
        author : data.author,
        content : data.content,
        url : data.url,
        publishedAt : data.publishedAt,
        showDetails: true
      }) 
    })
    .catch((error) => {
        console.log(error);
    })   
  }

    render() {
      return (
        <Container>
        <Row>&nbsp;</Row>
            <h1>
                <Badge variant="secondary">My Saved Headlines</Badge>
            </h1>       
        <Row>
            <Col xs={12} md={8}>
            <ListGroup>
              {   
              this.state.articles.map((article) => {              
                  return(<ListGroup.Item key={article._id} >{article.title} <Button variant="outline-info" onClick={this.onArticleClick.bind(this, article._id)} style={{float: "right"}}>Details</Button></ListGroup.Item>)
              })
              }
            </ListGroup>
            </Col>
            <Col xs={6} md={4}>
            {
            this.state.showDetails ? 
            (<Form>
                <h5>
                  Headlines Details
                </h5>
                <Form.Group as={Row} controlId="formTitle">
                  <Form.Label column sm="2">
                    Title
                  </Form.Label>
                  <Col sm="10">
                    <Form.Control type="text" readOnly  value={this.state.title || ''}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formAuthor">
                  <Form.Label column sm="2">
                    Author
                  </Form.Label>
                  <Col sm="10">
                    <Form.Control type="text" readOnly  value={this.state.author || ''}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formContent">
                  <Form.Label column sm="2">
                   Content
                  </Form.Label>
                  <Col sm="10">
                    <Form.Control type="text" readOnly  value={this.state.content || ''}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formURL">
                  <Form.Label column sm="2">
                    URL
                  </Form.Label>
                  <Col sm="10">
                    <Form.Control type="text" readOnly value={this.state.url || ''}/>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPublishedAt">
                  <Form.Label column sm="2">
                   Date
                  </Form.Label>
                  <Col sm="10">
                    <Form.Control type="text" readOnly value={this.state.publishedAt || ''}/>
                  </Col>
                </Form.Group>
              </Form>)
              : <div></div>
            }
            </Col>
        </Row>
        <Row>
            &nbsp;
        </Row>
        
        </Container>
      )
    }

}

export default NewsViewer;