import axios from 'axios';
import {CONFIG} from '../constants'

export function postHeadlinesReq (requestBody) {

    return new Promise((resolve, reject) => {
    
        const headers = {
            'Content-Type': 'application/json'
        }
            
        axios.post(CONFIG.ARTICLE_URL, requestBody, {headers : headers})       
        .then(response => {           
            resolve(response.data.message);                
        })
        .catch(error => {
            reject(error.response.data);
        });    

    });
}

export function getStoredHeadlinesReq (id) {

    return new Promise((resolve, reject) => {

        let url = id !== null ? `${CONFIG.ARTICLE_URL}${id}` : CONFIG.ARTICLE_URL;

        axios.get(url)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {            
                reject(error);
            })   
    });
}

export function getSourcesReq (searchString) {

    return new Promise((resolve, reject) => {

        let newSources = [];
        axios.get(`${CONFIG.SOURCES_URL}${searchString}`)
            .then(function (response) {
               
                response.data.sources.map((sources) => {
                    newSources.push(sources);
                    return newSources;
                });

                resolve(newSources);
            })
            .catch(function (error) {            
                reject(error);
            })   
    });
}

export function getHeadlinesReq (searchString) {

    return new Promise((resolve, reject) => {

        axios.get(`${CONFIG.TOP_HEADLINES_URL}${searchString}`)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {            
                reject(error);
            })   
    });
}